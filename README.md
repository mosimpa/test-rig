# MoSimPa's test rig controller software

This repository contains [MoSimPa](http://mosimpa.gitlab.io/)'s software to
control the test rig during production.

It makes use of [GIT flow](https://nvie.com/posts/a-successful-git-branching-model/), so remember:

- master is the production branch.
- develop is the actual development branch from which you should start working
  from.

Happy hacking!
