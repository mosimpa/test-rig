#!/bin/python3

from colorama import import init,  Style
import serial
import sys

def error(message):
    print(Fore.RED + message)
    raise Exception(message)

def fatalerror(message):
    print(Fore.MAGENTA + message)
    exit(100)

def logtitle(message):
    print(Style.BRIGHT + message)

def comm_init(port):
    print(Style.BRIGHT + 'Initializing communications port...')
    programmer_serial = serial.Serial(baudrate=115200, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE,
                                      stopbits=serial.STOPBITS_ONE, xonxoff=False, rtscts=False, timeout=True, port=port)
    if programmer_serial.isOpen() is False:
        error("The communications's serial port is not open.")
    print("Communications serial port init OK")
    return programmer_serial

def FT2232H_detect_ports():
    result = serial.tools.list_ports.comports()
    ports = [port for port in result if port.vid == 0x0403 and port.pid == 0x6010]

    if (len(ports) == 0):
        fatalerror("ERROR: FT2232H MiniModule not connected or not found. Run 'python -m serial.tools.list_ports -v' to check devices.")

    ports.sort(key = lambda x: x.device)
    return ports

def esp32_erase_flash(port):
    logtitle("## Erasing ESP32's flash")
    systemcall("esptool.py --port %s erase_flash" % port)

def esp32_flash(port, firmware):
    logtitle("## Programming test firmware to ESP32")
    # ToDo: check the real command line to be used.
    #systemcall("esptool.py --port {} --baud 576000 write_flash --flash_mode dio --flash_size=detect --verify 0 {}".format(port, firmware))

def main():
    # Detect USB-Serial device and it's ports.
    FT2232H_ports = FT2232H_detect_ports()
    programmer_port = FT2232H_ports[0]

    comm = comm_init(FT2232H_ports[1])

    try:
        comm.open()

        # Program the test firmware.
        esp32_erase_flash(programmer_port)
        esp32_flash(programmer_port, 'test_firmware.bin')

    except BaseException as e:
        comm.close()
        print(e)
        exit(1)

if __name__ == '__main__':
    main()
